package com.tobaconsulting.smartcar20.fragment;


import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.tobaconsulting.smartcar20.Class.StaticData;
import com.tobaconsulting.smartcar20.Class.Token;
import com.tobaconsulting.smartcar20.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment implements
        GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, View.OnClickListener, LocationListener {

    // URL = https://maps.googleapis.com/maps/api/directions/json?origin=hotel%20indonesia&destination=gereja%20katedral&key=AIzaSyA5gVnNsjFR7ZRHh9pPseshZFbdBzvtjHU
    private static String URL = "https://maps.googleapis.com/maps/api/directions/json?";
    private static String KEY = "AIzaSyA5gVnNsjFR7ZRHh9pPseshZFbdBzvtjHU";
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest locationRequest;
    private boolean mRequest = false;
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");
    private Location mLastLocation;
    private boolean StartLocation = true;

    public DashboardFragment() {
        // Required empty public constructor
    }

    public static final String TAG = DashboardFragment.class.getSimpleName();

    @Bind(R.id.txtUsername)
    TextView mTextUsername;
    @Bind(R.id.buttonTracking)
    Button mButtonTrack;

    public void setUsername() {
        OkHttpClient client = new OkHttpClient();
        String token = "";
        try {
            token = new Token(getContext()).getToken();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (token.equals("")) {

        } else {
            Request request = new Request.Builder()
                    .url(new StaticData().getValidateUserUrl())
                    .addHeader("origin", "android")
                    .addHeader("X-AUTH-TOKEN", token)
                    .build();
            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {
                        String data = response.body().string();
                        try {
                            JSONObject obj = new JSONObject(data);
                            final String username = obj.getString("username");
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mTextUsername.setText(username);
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ButterKnife.bind(this, view);

        setUsername();
        mButtonTrack.setOnClickListener(this);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    public void createLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(30000)
                .setFastestInterval(10000)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void getLocationUpdates() {
        createLocationRequest();

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi
                .requestLocationUpdates(mGoogleApiClient, locationRequest, this);
    }

    protected void stopLocationUpdate() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.buttonTracking: {
                if (!mRequest) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                            .setTitle("Sure?")
                            .setMessage("Are you sure you want to listen location?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Log.i(TAG, "Button Enable Pressed!");
                                    mRequest = true;
                                    StartLocation = true;
                                    mButtonTrack.setText(R.string.text_stop_tracking);
                                    getLocationUpdates();
                                }
                            })
                            .setNegativeButton("No", null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                            .setTitle("Sure?")
                            .setMessage("Are you sure you want to stop listen location?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Log.i(TAG, "Button Disable Pressed!");
                                    mRequest = false;
                                    stopLocationUpdate();
                                    mButtonTrack.setText(getString(R.string.text_start_tracking));
                                }
                            })
                            .setNegativeButton("No", null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            Toast.makeText(getActivity(), "Lat: "+location.getLatitude()+", Lng: "+location.getLongitude(), Toast.LENGTH_SHORT).show();
            Log.i(TAG, location.toString());
            if (StartLocation) {
                mLastLocation = location;
                StartLocation = false;
            }

            String origin = mLastLocation.getLatitude()+","+mLastLocation.getLongitude();
            String destination = location.getLatitude()+","+location.getLongitude();
            int dis = getDirectionDistance(origin, destination);

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

            JSONObject objToSend = new JSONObject();
            try {
                objToSend.put("timetrip", formatter.format(location.getTime()));
                objToSend.put("longitude", location.getLongitude());
                objToSend.put("latitude", location.getLatitude());
                objToSend.put("tripmeter", dis);
                objToSend.put("currentspeedgps", location.getSpeed());
                objToSend.put("prevlongitude", mLastLocation.getLongitude());
                objToSend.put("prevlatitude", mLastLocation.getLatitude());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.i(TAG, objToSend.toString());

            String token = "";
            try {
                token = new Token(getContext()).getToken();
            } catch (IOException e) {
                e.printStackTrace();
            }

            OkHttpClient client = new OkHttpClient();
            RequestBody body = RequestBody.create(JSON, objToSend.toString());
            Request request = new Request.Builder()
                    .url(new StaticData().getUrlSendLocation())
                    .addHeader("X-AUTH-TOKEN", token)
                    .addHeader("origin", "android")
                    .post(body)
                    .build();
            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {
                        Log.i(TAG, "Status : "+response.code());
                        Log.i(TAG, response.body().string());
                    }
                }
            });

//            Log.i(TAG, objToSend.toString());

            mLastLocation = location;
        }
    }

    private int getDirectionDistance(String origin, String destination) {
        String URLRequest = URL + "origin="+origin+"&destination="+destination+"&key="+KEY;

        Log.i(TAG, URLRequest);
        final int[] distance = {0};

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(URLRequest)
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    String data = response.body().string();
                    try {
                        JSONObject obj = new JSONObject(data);
                        JSONArray jsonRoutes = obj.getJSONArray("routes");
                        JSONObject objRoutes = jsonRoutes.getJSONObject(0);
                        JSONArray jsonLegs = objRoutes.getJSONArray("legs");
                        JSONObject jsonLeg = jsonLegs.getJSONObject(0);
                        JSONObject jsonDistance = jsonLeg.getJSONObject("distance");
                        distance[0] = jsonDistance.getInt("value");
                        // Log.i(TAG, "Distance = "+ distance[0]);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    // Log.i(TAG, "onResponse: "+data);

                }
            }
        });


        return distance[0];
    }
}
