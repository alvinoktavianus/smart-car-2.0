package com.tobaconsulting.smartcar20.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.tobaconsulting.smartcar20.AdapterClass.DividerItemDecoration;
import com.tobaconsulting.smartcar20.AdapterClass.Vehicle;
import com.tobaconsulting.smartcar20.AdapterClass.VehicleAdapter;
import com.tobaconsulting.smartcar20.Class.StaticData;
import com.tobaconsulting.smartcar20.Class.Token;
import com.tobaconsulting.smartcar20.CurrentTripActivity;
import com.tobaconsulting.smartcar20.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class VehicleFragment extends Fragment {

    public interface ClickListener {
        void onClick(View view, int position);
        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private VehicleFragment.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final VehicleFragment.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener(){
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    private List<Vehicle> vehicleList = new ArrayList<>();
    private VehicleAdapter mAdapter;
    @Bind(R.id.recycler_view_vehicle)RecyclerView mRecyclerView;

    private ArrayList<String> tripHistory = new ArrayList<>();
    private ArrayList<String> currTrip = new ArrayList<>();

    public static final String TAG = VehicleFragment.class.getSimpleName();

    public VehicleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_vehicle, container, false);
        ButterKnife.bind(this, rootView);

        try {
            String token = new Token(getActivity()).getToken();
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(new StaticData().getUrlAsset())
                    .addHeader("origin", "android")
                    .addHeader("X-AUTH-TOKEN", token)
                    .build();
            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    try {
                        if (response.isSuccessful()) {
                            String jsonData = response.body().string();
                            Log.i(TAG, jsonData);
                            ProcessJSON(jsonData);
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    mAdapter = new VehicleAdapter(vehicleList);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
                                    mRecyclerView.setLayoutManager(mLayoutManager);
                                    mRecyclerView.setItemAnimator(new DefaultItemAnimator());
                                    mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
                                    mRecyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), mRecyclerView, new ClickListener() {
                                        @Override
                                        public void onClick(View view, int position) {
                                            Toast.makeText(getContext(), currTrip.get(position), Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(getActivity(), CurrentTripActivity.class);
                                            intent.putExtra("currTrip", currTrip.get(position));
                                            intent.putExtra("tripHistory", tripHistory.get(position));
                                            startActivity(intent);
                                        }

                                        @Override
                                        public void onLongClick(View view, int position) {

                                        }
                                    }));
                                    mRecyclerView.setAdapter(mAdapter);
                                }
                            });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        response.body().close();
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void ProcessJSON(String jsonData) throws JSONException {
        JSONObject object = new JSONObject(jsonData).getJSONObject("_embedded");
        JSONArray array = object.getJSONArray("vehicleAssetList");
        for (int i=0; i<array.length(); i++) {
            JSONObject objInArray = array.getJSONObject(i);
            Vehicle vehicle = new Vehicle(objInArray.getString("plateno"), objInArray.getString("description"));
            vehicleList.add(vehicle);
            currTrip.add(objInArray.getJSONObject("_links").getJSONObject("CurrTrip").getString("href"));
            tripHistory.add(objInArray.getJSONObject("_links").getJSONObject("TripHistory").getString("href"));
        }
    }
}
