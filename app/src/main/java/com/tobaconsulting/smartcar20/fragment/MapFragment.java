package com.tobaconsulting.smartcar20.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tobaconsulting.smartcar20.Class.Token;
import com.tobaconsulting.smartcar20.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Alvin on 4/21/2016.
 */
public class MapFragment extends SupportMapFragment {

    public static final String TAG = MapFragment.class.getSimpleName();

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String url = getActivity().getIntent().getStringExtra("currTrip");
        try {
            String token = new Token(getContext()).getToken();

            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(url)
                    .addHeader("origin", "android")
                    .addHeader("X-AUTH-TOKEN", token)
                    .build();
            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {
                        String data = response.body().string();
                        Log.i(TAG, data);
                        try {
                            JSONObject obj = new JSONObject(data);
                            final float lat = Float.parseFloat(String.valueOf(obj.get("latitude")));
                            final float lng = Float.parseFloat(String.valueOf(obj.get("longitude")));
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    initCamera(lat, lng);
                                    initMarker(lat, lng);
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initMarker(float lat, float lng) {
        getMap().addMarker(new MarkerOptions().position(new LatLng(lat, lng)).title("Your Current Location"));
    }

    private void initCamera(float lat, float lng) {
        CameraPosition position = new CameraPosition.Builder()
                .target(new LatLng(lat, lng))
                .zoom(16f)
                .bearing(0.0f)
                .tilt(0.0f)
                .build();
        getMap().animateCamera(CameraUpdateFactory.newCameraPosition(position), null);
        getMap().setMapType(GoogleMap.MAP_TYPE_NORMAL);
    }
}
