package com.tobaconsulting.smartcar20;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.tobaconsulting.smartcar20.Class.Token;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class CurrentTripActivity extends AppCompatActivity {

    public static final String TAG = CurrentTripActivity.class.getSimpleName();
    @Bind(R.id.txtUsername)TextView mTxtDistance;
    @Bind(R.id.txtDate)TextView mTxtDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_trip);
        ButterKnife.bind(this);

        String token = "";
        try {
            token = new Token(getApplicationContext()).getToken();
        } catch (IOException e) {
            e.printStackTrace();
        }

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(getIntent().getStringExtra("currTrip"))
                .addHeader("origin", "android")
                .addHeader("X-AUTH-TOKEN", token)
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    String data = response.body().string();
                    Log.i(TAG, data);
                    try {
                        JSONObject obj = new JSONObject(data);
                        final String date = (String) obj.get("timetrip");
                        final String tripmeter = String.valueOf(obj.get("tripmeter"));
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mTxtDate.setText(date);
                                mTxtDistance.setText(tripmeter);
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_current_trip, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_history: {
                Toast.makeText(this, getIntent().getStringExtra("tripHistory"), Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(CurrentTripActivity.this, MapsActivity.class);
                intent.putExtra("tripHistory", getIntent().getStringExtra("tripHistory"));
                startActivity(intent);
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
