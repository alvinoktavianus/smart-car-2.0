package com.tobaconsulting.smartcar20;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.tobaconsulting.smartcar20.Class.StaticData;
import com.tobaconsulting.smartcar20.Class.Token;

import org.json.JSONException;

import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/*
* Source : http://www.androidhive.info/2015/09/android-material-design-floating-labels-for-edittext/
* Layout source : http://sourcey.com/beautiful-android-logn-and-signup-screens-with-material-design/screenshot-login.png
* */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    public static final MediaType MEDIA_TYPE
            = MediaType.parse("text/x-markdown; charset=utf-8");
    public static final String TAG = LoginActivity.class.getSimpleName();

    @Bind(R.id.button_login)Button mButtonSignIn;
    @Bind(R.id.button_sign_up)Button mButtonSignUp;
    @Bind(R.id.input_layout_username)TextInputLayout mLayoutUsername;
    @Bind(R.id.input_layout_password)TextInputLayout mLayoutPassword;
    @Bind(R.id.input_username)EditText mUsername;
    @Bind(R.id.input_password)EditText mPassword;
    @Bind(R.id.pbLoading)ProgressBar mPbLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mButtonSignIn.setOnClickListener(this);
        mButtonSignUp.setOnClickListener(this);

        mUsername.addTextChangedListener(new MyTextWatcher(mUsername));
        mPassword.addTextChangedListener(new MyTextWatcher(mPassword));
    }

    /*
    * Validating Form
    * */

    private void submitForm() throws JSONException {
        if (!validateUsername())
            return;
        if (!validatePassword())
            return;
        String data = new StaticData().getJSONLogin(mUsername.getText().toString().trim(), mPassword.getText().toString().trim());
        Log.i(TAG, data);

        mLayoutUsername.setVisibility(View.INVISIBLE);
        mLayoutPassword.setVisibility(View.INVISIBLE);
        mButtonSignIn.setVisibility(View.INVISIBLE);
        mButtonSignUp.setVisibility(View.INVISIBLE);
        mPbLoading.setVisibility(View.VISIBLE);

        OkHttpClient client = new OkHttpClient();
        RequestBody body = RequestBody.create(MEDIA_TYPE, data);
        Request request = new Request.Builder()
                .url(new StaticData().getLoginUrl())
                .addHeader("origin", "android")
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    if (response.isSuccessful()) {
                        Log.i(TAG, response.header("X-AUTH-TOKEN"));
                        new Token(getApplicationContext()).setToken(response.header("X-AUTH-TOKEN"));
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Please verify username and password!", Toast.LENGTH_SHORT).show();
                                mPbLoading.setVisibility(View.INVISIBLE);
                                mLayoutUsername.setVisibility(View.VISIBLE);
                                mLayoutPassword.setVisibility(View.VISIBLE);
                                mButtonSignIn.setVisibility(View.VISIBLE);
                                mButtonSignUp.setVisibility(View.VISIBLE);

                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    response.body().close();
                }
            }
        });
    }

    private boolean validateUsername() {
        if (mUsername.getText().toString().trim().isEmpty()){
            mLayoutUsername.setError("Please enter username!");
            requestFocus(mUsername);
            return false;
        } else {
            mLayoutUsername.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validatePassword() {
        if (mPassword.getText().toString().trim().isEmpty()) {
            mLayoutPassword.setError("Please enter password!");
            requestFocus(mPassword);
            return false;
        } else {
            mLayoutPassword.setErrorEnabled(false);
        }
        return true;
    }

    private void requestFocus(View view){
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        public MyTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            switch (view.getId()) {
                case R.id.input_username:
                    validateUsername();
                    break;
                case R.id.input_password:
                    validatePassword();
                    break;
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (view.getId()) {
                case R.id.input_username:
                    validateUsername();
                    break;
                case R.id.input_password:
                    validatePassword();
                    break;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_login:{
                try {
                    submitForm();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            }
            case R.id.button_sign_up: {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
                break;
            }
        }
    }
}
