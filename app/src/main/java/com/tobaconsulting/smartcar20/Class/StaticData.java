package com.tobaconsulting.smartcar20.Class;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Alvin on 4/20/2016.
 */
public class StaticData {

    public static final String URL = "http://smartcar.tobaconsulting.com:9999/api/v1";

    public StaticData() {
    }

    public String getJSONLogin(String username, String password) throws JSONException {
        JSONObject object = new JSONObject();
        object.put("username", username);
        object.put("password", password);
        return object.toString();
    }

    public String getLoginUrl(){ return URL+"/login"; }
    public String getUrlAsset() { return URL+"/vehicleasset/all"; }
    public String getValidateUserUrl() { return URL+"/users/current"; }
    public String getUrlSendLocation() { return URL+"/assetrip/sendlocationuser"; }
    public String getUrlBrandType() { return URL+"/users/registration/listbrandtype"; }

}
