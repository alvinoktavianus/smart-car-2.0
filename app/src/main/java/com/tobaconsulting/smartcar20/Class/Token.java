package com.tobaconsulting.smartcar20.Class;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Alvin on 4/20/2016.
 */
public class Token {

    private Context context;

    public Token(Context context) {
        this.context = context;
    }

    public Token() {

    }

    public void setToken(String token) throws IOException {
        FileOutputStream outputStream = context.getApplicationContext().openFileOutput("token", Context.MODE_PRIVATE);
        outputStream.write(token.getBytes());
        outputStream.close();
    }

    public String getToken() throws IOException {
        String token = "";
        int c;
        FileInputStream inputStream = context.getApplicationContext().openFileInput("token");
        while ( (c=inputStream.read()) != -1 ) {
            token += Character.toString((char)c);
        }
        return token;
    }
}
