package com.tobaconsulting.smartcar20;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.tobaconsulting.smartcar20.Class.StaticData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class SignUpActivity extends AppCompatActivity {

    public static final String TAG = SignUpActivity.class.getSimpleName();
    @Bind(R.id.spinnerCarBrand)Spinner mSpinnerCarBrand;
    ArrayList<String> listData = new ArrayList<>();
    ArrayList<String> listBrand = new ArrayList<>();
    ArrayList<Integer> listBrandId = new ArrayList<>();

    int brandPosition = 0;
    int modelPosition = 0;

    OkHttpClient client = new OkHttpClient();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);

        LoadCarBrand();

    }

    public void LoadCarBrand() {
        Request request = new Request.Builder()
                .url(new StaticData().getUrlBrandType())
                .addHeader("origin", "android")
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    String data = response.body().string();
                    Log.i(TAG, data);
                    try {
                        JSONObject obj = new JSONObject(data);
                        JSONArray arrayBrand = obj.getJSONArray("vehicleBrands");
                        for (int i=0; i<arrayBrand.length(); i++) {
                            listBrand.add(arrayBrand.getJSONObject(i).getString("description"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    public class MyAdapter extends ArrayAdapter<String> {


        public MyAdapter(Context context, int resource, List<String> objects) {
            super(context, resource, objects);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return super.getDropDownView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return super.getView(position, convertView, parent);
        }

        @Bind(R.id.textData)TextView mTextData;
        public View getCustomView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = getLayoutInflater();
            View mySpinner = inflater.inflate(R.layout.spinner_list_row, parent, false);
            ButterKnife.bind(this, mySpinner);

            mTextData.setText(listData.get(position));

            return mySpinner;
        }
    }
}
