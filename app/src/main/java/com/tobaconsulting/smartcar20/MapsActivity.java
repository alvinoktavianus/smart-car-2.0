package com.tobaconsulting.smartcar20;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.tobaconsulting.smartcar20.Class.Token;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    // URL = https://maps.googleapis.com/maps/api/directions/json?origin=hotel%20indonesia&destination=gereja%20katedral&key=AIzaSyA5gVnNsjFR7ZRHh9pPseshZFbdBzvtjHU
    private static String URL = "https://maps.googleapis.com/maps/api/directions/json?origin=";
    private static String KEY = "AIzaSyA5gVnNsjFR7ZRHh9pPseshZFbdBzvtjHU";
    public static final String TAG = MapsActivity.class.getSimpleName();
    PolylineOptions polylineOptions = new PolylineOptions();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng Jakarta = new LatLng(6.10, 106.48);
        // mMap.moveCamera(CameraUpdateFactory.newLatLng(Jakarta));

        String url = getIntent().getStringExtra("tripHistory");
        String token = "";
        try {
            token = new Token(getApplicationContext()).getToken();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(url)
                    .addHeader("origin", "android")
                    .addHeader("X-AUTH-TOKEN", token)
                    .build();
            Call call = client.newCall(request);
            call.enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {
                        String data = response.body().string();
                        try {
                            JSONArray array = new JSONArray(data);
                            int args = 0;
                            for (int i=0; i<array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);
                                final Float lat = Float.parseFloat(object.getString("latitude"));
                                final Float lng = Float.parseFloat(object.getString("longitude"));
                                args = i;
                                final int finalArgs = args;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (finalArgs == 1) {
                                            CameraPosition position = new CameraPosition.Builder()
                                                    .target(new LatLng(lat, lng))
                                                    .zoom(10f)
                                                    .bearing(0.0f)
                                                    .tilt(0.0f)
                                                    .build();
                                            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
                                        }
                                        addMarker(lat, lng, finalArgs);
                                    }
                                });
                                addLine(lat, lng);
                            }
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Polyline polyline = mMap.addPolyline(polylineOptions);
                                }
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    private void addLine(Float lat, Float lng) {
        polylineOptions.add(new LatLng(lat, lng));
    }

    private void addMarker(Float lat, Float lng, int i) {
        LatLng location = new LatLng(lat, lng);
        mMap.addMarker(new MarkerOptions().position(location).title("Marker "+(i+1)));
    }
}
