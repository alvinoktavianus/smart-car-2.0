package com.tobaconsulting.smartcar20.AdapterClass;

/**
 * Created by Alvin on 4/25/2016.
 */
public class Settings {

    private String setting;

    public Settings(String setting) {
        this.setting = setting;
    }

    public String getSetting() {
        return setting;
    }

    public void setSetting(String setting) {
        this.setting = setting;
    }
}
