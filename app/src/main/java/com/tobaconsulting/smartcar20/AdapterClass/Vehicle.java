package com.tobaconsulting.smartcar20.AdapterClass;

/**
 * Created by Alvin on 4/20/2016.
 */
public class Vehicle {
    private String plateno, description;

    public Vehicle(){
        super();
    }

    public Vehicle(String plateno, String description) {
        this.plateno = plateno;
        this.description = description;
    }

    public String getPlateno() {
        return plateno;
    }

    public void setPlateno(String plateno) {
        this.plateno = plateno;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
