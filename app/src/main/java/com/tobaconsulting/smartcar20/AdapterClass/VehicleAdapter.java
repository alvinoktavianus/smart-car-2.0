package com.tobaconsulting.smartcar20.AdapterClass;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tobaconsulting.smartcar20.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Alvin on 4/21/2016.
 */
/**
 * Source : http://www.androidhive.info/2016/01/android-working-with-recycler-view/
 * */
public class VehicleAdapter extends RecyclerView.Adapter<VehicleAdapter.MyViewHolder> {

    private List<Vehicle> vehicleList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.plateno)TextView plateno;
        @Bind(R.id.description)TextView description;
        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public VehicleAdapter(List<Vehicle> vehicleList) {
        this.vehicleList = vehicleList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.vehicle_list_row, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Vehicle vehicle = vehicleList.get(position);
        holder.plateno.setText(vehicle.getPlateno());
        holder.description.setText(vehicle.getDescription());
    }

    @Override
    public int getItemCount() {
        return vehicleList.size();
    }
}
